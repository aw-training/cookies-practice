<?php
session_start();
$cookie_value=$_POST["name"];

  if (empty($_POST["name"])) {
    $_SESSION["nameErr"]= "* Name is required";
  } else {
    $_SESSION["nameErr"]="";
    $_SESSION["name"] = $_POST["name"];
  }
  
  if (empty($_POST["email"])) {
    $_SESSION["emailErr"] = "* Email is required";
  } else {
    $_SESSION["emailErr"]="";
    $_SESSION["email"] = $_POST["email"];
  }
    
  if (empty($_POST["website"])) {
    $_SESSION["websiteErr"] = "";
    $_SESSION["website"]="No website added";
  } else {
    $_SESSION["website"] = $_POST["website"];
  }

  if (empty($_POST["comment"])) {
    $_SESSION["commentErr"] = "";
    $_SESSION["comment"]="No comments added";
  } else {
    $_SESSION["comment"] = $_POST["comment"];
  }

  if (empty($_POST["gender"])) {
    $_SESSION["genderErr"] = "* Gender is required";
  } 
  else{
    $_SESSION["genderErr"]="";
    $gender=$_POST["gender"];
    if($gender=="female"){
      $_SESSION["gender"] = "female";
    }
    elseif($gender=="male"){
      $_SESSION["gender"] = "male";
    }
  }
$fileName = 'data.txt';
$file = fopen($fileName,"a") or die("Unable to open file!");
$storage_data.= ''."NAME:     ".$_SESSION["name"].'
'."E-MAIL:   ".$_SESSION["email"].'
'."WEBSITE:  ".$_SESSION["website"].'
'."COMMENTS: ".$_SESSION["comment"].'
'."GENDER:   ".$_SESSION["gender"]."\n".'
';

if((!empty($_POST["name"])) && (!empty($_POST["email"])) && (!empty($_POST["gender"]))){
  fwrite($file, $storage_data) or $_SESSION["storage"]="ERROR IN STORING DATA, PLEASE TRY AGAIN";  
  setcookie("user",$cookie_value, time()+86400);
  $_SESSION["storage"]="DATA STORED SUCCESSFULLY";
} else{
  $_SESSION["storage"]="";
}
fclose($file);
header("Location: index.php");