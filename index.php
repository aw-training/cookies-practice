<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title></title>
  <style type="text/css">
  input[type=text], select, textarea {
    width: 100%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    resize: vertical;
  }
  label {
    padding: 12px 12px 12px 0;
    display: inline-block;
  }
  input[type=submit] {
    background-color: #3366ff;
    color: white;
    font-size: 16px;
    padding: 10px 30px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    float: right;
  }
  input[type=submit]:hover {
    background-color:#0040ff;
  }
  .hellouser{
    margin:auto;
    width:60%;
    font-size: 25px;
    padding: 20px 0 0 0;
  }
  .container {
    margin:40px auto;
    width:60%;
    border-radius: 5px;
    background-color: #809fff;
    padding: 20px;
  }
  .col-25 {
    float: left;
    width: 25%;
    margin-top: 6px;
  }
  .col-75 {
    float: left;
    width: 71%;
    margin-top: 6px;
  }
  .row:after {
    content: "";
    display: table;
    clear: both;
  }
  @media screen and (max-width: 600px) {
    .col-25, .col-75, input[type=submit] {
      width: 100%;
      margin-top: 0;
    }
  }
  .smalltext{
    font-size: 15px;
    color:  #009900;
  }
  .error{
    color:red;
  }
  .errorbox{
    padding: 5px;
    margin-top: 10px;
  }
  </style>
</head>
<body>
<div class="hellouser">
    <?php
    if(isset($_COOKIE['user'])) echo "WELCOME ".$_COOKIE['user'].'<br>';
    if(isset($_SESSION["storage"])) {
      echo '<span class="smalltext">'.$_SESSION["storage"].'</span>';
    }
    ?>
</div>
<div class="container">
<form method="post" action="validation.php">
  <div class="row">
    <div class="col-25">
      <label for="name">NAME:</label>
    </div>
    <div class="col-75">
      <input type="text" id="name" name="name" columns="40"><br>
      <span class="error"><?php if(isset($_SESSION["nameErr"])){echo $_SESSION["nameErr"];}?></span>
    </div>
  </div>  
  <div class="row">
    <div class="col-25">
      <label for="email">EMAIL:</label>
    </div>
    <div class="col-75">
      <input type="text" id="email" name="email" ><br>
      <span class="error"><?php if(isset($_SESSION["emailErr"])){echo $_SESSION["emailErr"];}?></span>
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="website">WEBSITE:</label>
    </div>
    <div class="col-75">
      <input type="text" id="website" name="website" ><br>
      <span class="error"> <?php if(isset($_SESSION["websiteErr"])){echo $_SESSION["websiteErr"];}?></span>
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="subject">COMMENTS:</label>
    </div>
    <div class="col-75">
      <textarea id="subject" name="comment" style="height:80px"></textarea>
    </div>
    <span class="error"><?php if(isset($_SESSION["commentErr"])){echo $_SESSION["commentErr"];}?></span>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="gender">GENDER:</label>
    </div>
    <div class="col-75">
      <input type="radio" id="male" name="gender" value="male">
      <label for="male">MALE</label>
      <input type="radio" id="female" name="gender" value="female">
      <label for="female">FEMALE</label>
      <input type="radio" id="other" name="gender" value="other">
      <label for="other">OTHER</label><br>
      <span class="error"><?php if(isset($_SESSION["genderErr"])){echo $_SESSION["genderErr"];}?></span>
    </div>
  </div>
  <div class="row">
    <input type="submit" value="Submit">
  </div>
</form>
</div>

</body>
</html>
<?php
session_unset();
?>
